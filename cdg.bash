#!/bin/bash

cdg() {
	local dir_to_go=$(cat "$HOME/.bams.cdg/bookmarks" | sed '/^\s*$/d' | fzf)

	[[ -z "${dir_to_go}" ]] && return 0

	if [ -d "${dir_to_go}" ]; then
		cd "${dir_to_go}"
	else
		echo "Path not exist: $dir_to_go"
	fi
} 

cdb() {
	local path=$(pwd)

	if grep -Fxq "${path}" "$HOME/.bams.cdg/bookmarks"; then
		echo "Already bookmarked: ${path}"
		return 0
 	fi

	echo "${path}" >> "$HOME/.bams.cdg/bookmarks" && echo "Added to bookmarks: ${path}"
}

cdd() {
	local path=$(pwd)
	
	local qry=""
	if grep -Fxq "${path}" "$HOME/.bams.cdg/bookmarks"; then
		qry="-q${path}"
 	fi

	local dir_to_remove=$(cat "$HOME/.bams.cdg/bookmarks" | sed '/^\s*$/d' | fzf $qry)

	local dir_to_remove_escaped=$(echo "${dir_to_remove}" | sed 's/\//\\\//g')

	if [ -n "${dir_to_remove}" ]; then
		sed -i '/^'"${dir_to_remove_escaped}"'$/d' "$HOME/.bams.cdg/bookmarks" && echo "Removed from bookmarks: $dir_to_remove"
	else
		echo "Canceled"
	fi 
}

cde() {
	subl "$HOME/.bams.cdg/bookmarks"
}